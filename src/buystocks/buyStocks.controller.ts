import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { BuyStocksService } from './buyStocks.service';
import { CreateBuyStockDto } from './dto/create-buyStock.dto';
import { UpdateBuyStockDto } from './dto/update-buyStock.dto';

@Controller('buyStocks')
export class BuyStocksController {
  constructor(private readonly buyStocksService: BuyStocksService) {}
  // Create
  @Post()
  create(@Body() createBuyStockDto: CreateBuyStockDto) {
    return this.buyStocksService.create(createBuyStockDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.buyStocksService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.buyStocksService.findOne(+id);
  }
  // Partial Update
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBuyStockDto: UpdateBuyStockDto,
  ) {
    return this.buyStocksService.update(+id, updateBuyStockDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.buyStocksService.remove(+id);
  }
}
