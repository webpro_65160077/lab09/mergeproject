import { Injectable } from '@nestjs/common';
import { CreateBuyStockDto } from './dto/create-buyStock.dto';
import { UpdateBuyStockDto } from './dto/update-buyStock.dto';
import { BuyStock } from './entities/buyStock.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BuyStocksService {
  constructor(
    @InjectRepository(BuyStock)
    private buyStocksRepository: Repository<BuyStock>,
  ) {}

  create(createBuyStockDto: CreateBuyStockDto): Promise<BuyStock> {
    return this.buyStocksRepository.save(createBuyStockDto);
  }

  findAll(): Promise<BuyStock[]> {
    return this.buyStocksRepository.find();
  }

  findOne(id: number) {
    return this.buyStocksRepository.findOneBy({ id: id });
  }

  async update(id: number, updateBuyStockDto: UpdateBuyStockDto) {
    await this.buyStocksRepository.update(id, updateBuyStockDto);
    const buyStock = await this.buyStocksRepository.findOneBy({ id });
    return buyStock;
  }

  async remove(id: number) {
    const deleteBuyStock = await this.buyStocksRepository.findOneBy({ id });
    return this.buyStocksRepository.remove(deleteBuyStock);
  }
}
